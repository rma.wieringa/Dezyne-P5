/*
 * Copyright (C) 2021 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Dezyne-P5.
 * Dezyne-P5 offers Dezyne web views based on p5.js
 *
 * Dezyne-P5 is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class IframeP5 extends DiagramP5 {

  constructor(parent) {
    super(parent);
    /*
     * super defines interface to the outside world, through functions
     *   in.draw(data)
     *   in.dimensions(px, py, width, height)
     *   out.selected(location)
     *
     * extension: none
     */

    this.page = null;
  }

  // overrider super
  dimensions(px, py, width, height) {
    this.px = px;
    this.py = py;
    this.width = width;
    this.height = height;
    if (this.set_up) {
      this.page.position(px, py);
      this.page.attribute('width', width);
      this.page.attribute('height', height);
    }
  }

  initDiagram() {
    this.page.attribute('src', this.data);
    this.sketch.noLoop();
  }

  setup(p) {
    let px = this.px;
    let py = this.py;
    let w = this.width || p.windowWidth;
    let h = this.height || p.windowHeight;

    this.page = p.createElement('iframe');
    this.page.position(px, py);
    this.page.attribute('width', w);
    this.page.attribute('height', h);

    this.set_up = true;
    if (this.data) {
      this.initDiagram();
    }
    p.noLoop();
  }

  draw(p) {
  }

  mouseInCanvas(p) {
    if (!this.width && !this.height) return true; // canvas spans whole window
    if (this.width && !(0 <= p.mouseX && p.mouseX <= this.width)) return false;
    if (this.height && !(0 <= p.mouseY && p.mouseY <= this.height)) return false;
    return true;
  }

  windowResized(p) {
    if (!this.width && !this.height)
      this.world.resizeCanvas(this.windowWidth, this.windowHeight);
  }

  mousePressed(p) {
    if (!this.mouseInCanvas(p)) return;
  }

}

